import React, { Component, createRef } from "react";
// react bootstrap
import "bootstrap/dist/css/bootstrap.min.css";
// Custom styles css
import "./styles/quiz_test.css";
import { Container, Row, Col } from "react-bootstrap";
import { connect } from "react-redux";
// import {getQuiz, getQuiz1, getQuiz2, getQuiz3} from "../../../redux/actions/quiz/QuizAction";
import {getQuiz} from "../../../redux/actions/quiz/QuizAction";
import { bindActionCreators } from "redux";
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome'
import { faArrowAltCircleLeft, faArrowCircleRight } from '@fortawesome/free-solid-svg-icons'

class QuizTest extends Component {
  constructor()
  {
    super()
    this.state = {
      isFirstQuiz: true,
      isCheck: true,
      answersCount: {},
      result: '',
      realistic: ''
    }
    this.setState({
      realistic: 0
    })

    this.onChangeValue = this.onChangeValue.bind(this);

    this.mainData = []
    this.data = []
    this.data1 = []
    this.date2 = []
    this.data3 = []
    this.data4 = []
    this.count = 0
  }
  onChangeValue(event) {
    console.log(event.target.id);
  }
 
  click = () =>
  { 
    switch(this.count)
    { 
      // case 1: this.props.getQuiz1(); console.log("CASE1)"); 
      //         this.data1 = this.props.data; 
      //         this.data1.forEach(function (element) {
      //           element.check1 = "false";
      //           element.check2 = "false";
      //           element.check3 = "false";
      //         });
      //         console.log(this.data1);
      //         console.log("DATA1: ", this.data1); 
      //         this.mainData = this.data1; break;
      // case 2: this.props.getQuiz2(); console.log("CASE2"); break;
      // case 3: this.props.getQuiz3(); console.log("CASE3"); break;
      default: this.props.getQuiz(); console.log("DEFAULT");
    }
  }
  
  // handleNextClick = () =>{ 
  //   this.count +=1
  //   this.click()
  //   this.setState({
  //     isFirstQuiz: false,
  //   })
  // }

  componentDidMount() {
    this.props.getQuiz();
  }
 
  handleChange = (a) =>{
    console.log(a.target.id);
    // console.log(a.target.value);
    let realistic = 0;

    if(a.target.id === "Realistic"){
      realistic=parseInt(a.target.value, 10);
      console.log(realistic)
    }
    else if(a.target.id ==="Conventional"){
      alert(2);
    }else if(a.target.id ==="Investigate"){
      alert(3)
    }else if(a.target.id ==="Artistic"){
        alert(3);
    }else if(a.target.id ==="Social"){
      alert(4);
    }else if(a.target.id ==="Enterprising"){
      alert(5);
    }else{

    }
    
    switch(this.count)
    {
      case 0:
            switch(a.target.name)
            {
              case 1:
                    console.log(a.target.name);
                    break;
              case 2:
                    console.log(a.target.name);
                    break;
              case 3: 
                    console.log(a.target.name);
                    break;
              case 4: 
                    console.log(a.target.name);
                    break;
              default:
                    break;
            }
            break;
      default: 
            break;
    }
    
  }

  render() {                  
    this.data = this.props.data;
    this.data.forEach(function (element) {
      element.check1 = false;
      element.check2 = false;
      element.check3 = false;
    });
    console.log(this.props.data);
    console.log("DATA1: ", this.data); 
    this.mainData = this.data;
    return (
      <Container>
        <div className="header">
          <h3>Home &gt; Quiz</h3>
        </div>
        <div className="quiz">
          <div className="quiz_header">
            <h4>Quiz Test</h4>
          </div>
        </div>
        <div className="question">
          <h5>សូមជ្រើសរើសចម្លើយ​១នៅក្នុងរង្វង់មូល​ ៖</h5>
          <Row>
            {this.mainData.map((data, key) => {
              return (
                <Col md={6}>
                  <div>
                    <p>
                      {data.id}. {data.que}
                    </p>
                    <input
                      onClick={this.handleChange}
                      className="radio"
                      type="radio"
                      name={data.id}
                      id={data.type}
                      value="0"
                    />

                    <label for="male">មិនចូលចិត្ត</label>
                    <br />
                    <input 
                      onChange={this.handleChange}
                      id={data.type}
                      className="radio"
                      type="radio"
                      name={data.id}
                      value="1"
                    />

                    <label for="female">ចូលចិត្ត</label>
                    <br />
                    <input
                      onChange={this.handleChange}
                      id={data.type}
                      className="radio"
                      type="radio"
                      name={data.id}
                      value="2"
                    />
                    <label for="other">ចូលចិត្តណាស់</label>
                  </div>
                </Col>
              )
            })
          }
          </Row>
          {/* {
            this.count <= 0 ? <div className="quiz_footer_first"><button>
              <FontAwesomeIcon className="next" icon={faArrowCircleRight} size="3x" onClick={this.handleNextClick}/></button></div>:
            <div className="quiz_footer">
              <button><FontAwesomeIcon className="next_prev" icon={faArrowAltCircleLeft} size="3x" onClick={this.handlePrevClick}/> </button>
              <button><FontAwesomeIcon className="next1" icon={faArrowCircleRight} size="3x" onClick={this.handleNextClick}/> </button>
              
            </div>

          } */}
          <div className="quiz_footer">
            <button type="submit" style={{backgroundColor: 'green', color: '#fff', padding: '12px', borderRadius: '5px'}} onClick={(a) => this.handleChange(a)}>Submit</button>
          </div>
        </div> 
      </Container>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    data: state.quizReducer.data,
  };
};

const mapActionToProps = (dp) => {
  return bindActionCreators(
    {
      // getQuiz,getQuiz1,getQuiz2,getQuiz3
      getQuiz,
    },
    dp
  );
};

export default connect(mapStateToProps, mapActionToProps)(QuizTest);

 