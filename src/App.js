import React from 'react';
import './App.css';
import QuizTest from './components/client_side/quiz_test/QuizTest';

function App() {
  return (
    <QuizTest/>
  );
}

export default App;
