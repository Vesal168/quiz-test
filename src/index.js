import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import { createStore, applyMiddleware } from 'redux';
import { RootReducer } from './redux/reducers/RootReducer';
import thunk from 'redux-thunk';
import logger from 'redux-logger';
import { Provider } from 'react-redux';


const middleWare = [thunk, logger]
const store = createStore(RootReducer, applyMiddleware(...middleWare))

//const store = createStore(quizReducer)
ReactDOM.render(
  <Provider store = {store}>
    <React.StrictMode>
      <App />
    </React.StrictMode>
  </Provider>,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
