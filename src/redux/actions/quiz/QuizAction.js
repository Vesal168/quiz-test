import { GET_QUIZ, DELETE_QUIZ, PUT_QUIZ } from "./QuizActionType";

export const getQuiz = () => {
  return (dp) => {
    dp({
      type: GET_QUIZ,
      data: [{ id: 1, que: "Are you honest?", type: "Realistic" },
      { id: 2, que: "Are you honest?", type: "Realistic" },
      { id: 3, que: "Are you honest?", type: "Realistic" },
      { id: 4, que: "Are you get new item?", type: "Realistic" },
      { id: 5, que: "Are you to research?", type: "Realistic" },
      { id: 6, que: "Are you honest?", type: "Conventional" },
      { id: 7, que: "you helper person?", type: "Conventional" },
      { id: 8, que: "Are you honest?", type: "Conventional" },
      { id: 9, que: " Are you responsible work?", type: "Conventional" },
      { id: 10, que: " Are you obey person?", type: "Conventional" },
      { id: 11, que: "What is your favorith action in class ?", type: "Investigate" },
      { id: 12, que: "What is your favorith action in class ? ", type: "Investigate" },
      { id: 13, que: "What is your favorith action in class ?", type: "Investigate" },
      { id: 14, que: "What is your favorith action in class ?", type: "Investigate" },
      { id: 15, que: "What is your favorith action in class ?", type: "Investigate" },
      { id: 16, que: "What is your favorith action in class ?", type: "Artistic" },
      { id: 17, que: "What is your favorith action in class ?", type: "Artistic" },
      { id: 18, que: "What is your favorith action in class ?", type: "Artistic" },
      { id: 19, que: "What is your favorith action in class ?", type: "Artistic" },
      { id: 20, que: "What is your favorith action in class ?", type: "Artistic" },
      { id: 21, que: "What is your favorith action in class ?", type: "Social" },
      { id: 22, que: "What is your favorith action in class ?", type: "Social" },
      { id: 23, que: "What is your favorith action in class ?", type: "Social" },
      { id: 24, que: "What is your favorith action in class ?", type: "Social" },
      { id: 25, que: "What is your favorith action in class ?", type: "Social" },
      { id: 26, que: "What is your favorith action in class ?", type: "Enterprising" },
      { id: 27, que: "What is your favorith action in class ?", type: "Enterprising" },
      { id: 28, que: "What is your favorith action in class ?", type: "Enterprising" },
      { id: 29, que: "What is your favorith action in class ?", type: "Enterprising" },
      { id: 30, que: "What is your favorith action in class ?", type: "Enterprising" }
      ],
    });
  };
};


// export const getQuiz1 = () => {
//   return (dp) => {
//     dp({
//       type: GET_QUIZ,
//       data: [ 
//         {id:7, que:"What is your favorith action in class ?"},
//         {id:8, que:"What is your favorith action in class ?"},
//         {id:9, que:"What is your favorith action in class ?"},
//         {id:10, que:"What is your favorith action in class ?"},
//         {id:11, que:"What is your favorith action in class ?"},
//         {id:12, que:"What is your favorith action in class ?"} 
//       ]
//     });
//   };
// };


// export const getQuiz2 = () => {
//   return (dp) => {
//     dp({
//       type: GET_QUIZ,
//       data: [
//         {id:13, que:"What is your favorith action in class ?",},
//         {id:14, que:"What is your favorith action in class ?",},
//         {id:15, que:"What is your favorith action in class ?" },
//         {id:16, que:"What is your favorith action in class ?"},
//         {id:17, que:"What is your favorith action in class ?",},
//         {id:18, que:"What is your favorith action in class ?",} 
//       ],
//     });
//   };
// };


// export const getQuiz3 = () => {
//   return (dp) => {
//     dp({
//       type: GET_QUIZ,
//       data: [
//         {id:19, que:"What is your favorith action in class ?" ,},
//         {id:20, que:"What is your favorith action in class ?", },
//         {id:21, que:"What is your favorith action in class ?" ,},
//         {id:22, que:"What is your favorith action in class ?" ,},
//         {id:23, que:"What is your favorith action in class ?", },
//         {id:24, que:"What is your favorith action in class ?", } 
//       ],
//     });
//   };
// };