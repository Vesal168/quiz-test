import { GET_QUIZ } from "../../actions/quiz/QuizActionType";

const defaultState = 
{
    data: []
}

const quizReducer = (state = defaultState, action) =>
{
    switch(action.type)
    {
        case GET_QUIZ:
            return {
                ...state,
                data: action.data
            }
        default: 
            return state
    }
}

export default quizReducer