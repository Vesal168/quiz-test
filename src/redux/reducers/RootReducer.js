import { combineReducers } from "redux";
import quizReducer from "./quiz/QuizReducer";

const allReducers = { quizReducer };

export const RootReducer = combineReducers(allReducers);
